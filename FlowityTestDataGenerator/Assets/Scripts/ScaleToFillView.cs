﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleToFillView : MonoBehaviour {

	[SerializeField]
	private Camera targetCamera;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float pos = (targetCamera.nearClipPlane + 0.01f);

		transform.position = targetCamera.transform.position + targetCamera.transform.forward * pos;

		float h = Mathf.Tan(targetCamera.fieldOfView * Mathf.Deg2Rad * 0.5f) * pos * 2f;

		transform.localScale = new Vector3(h, 0f, h * targetCamera.aspect);
	}
}
