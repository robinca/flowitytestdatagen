﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PatrolRandomPointsOnNavMesh : MonoBehaviour {

	public delegate void PatrolEvent();
	public PatrolEvent PatrolPointFound;
	public PatrolEvent PatrolPointReached;

	[SerializeField]
	private float maxPatrolDistance = 10;
	[SerializeField]
	private float waitTimeOnPatrolPoint = 3;
	[SerializeField]
	private float maxPatrolTime = 10;

	private float currentWaitTime = 0;
	private float currentPatrolTime = 0;
	private NavMeshAgent navAgent;

	private PatrolState currentPatrolState;
	private enum PatrolState
	{
		Waiting,
		Walking
	}
	// Use this for initialization
	void Start () {
		currentPatrolState = PatrolState.Walking;
		navAgent = GetComponent<NavMeshAgent>();
		PatrolPointReached += OnPatrolPointReached;
		PatrolPointFound += OnPatrolPointFound;
		FindNewDestination();
	}
	
	// Update is called once per frame
	void Update () {
		if (!navAgent.hasPath && !navAgent.pathPending && navAgent.remainingDistance < navAgent.radius)
		{
			if(currentPatrolState.Equals(PatrolState.Walking))
				PatrolPointReached();
		}
		if (currentPatrolState.Equals(PatrolState.Waiting))
		{
			currentWaitTime += Time.deltaTime;
			if (currentWaitTime > waitTimeOnPatrolPoint)
			{
				currentWaitTime -= waitTimeOnPatrolPoint;
				PatrolPointFound();
			}
		}
	}
	private void OnPatrolPointReached()
	{
		currentPatrolState = PatrolState.Waiting;
	}
	private void OnPatrolPointFound()
	{
		FindNewDestination();
		currentPatrolState = PatrolState.Walking;
	}
	private void FindNewDestination()
	{
		var targetPoint = transform.position + 
			Random.insideUnitSphere * maxPatrolDistance;
		NavMeshHit hit;
		NavMesh.SamplePosition(targetPoint, out hit, maxPatrolDistance, 1);
		navAgent.destination = hit.position;
	}
}
