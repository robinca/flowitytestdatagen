﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SegmentationProperties))]
public class ApplySegmentationPropertyMaterialToRenderersInHierarchy : ApplyMaterialToRenderersInHierarchy
{

	// Use this for initialization
	void Start () {
		var segProperties = GetComponent<SegmentationProperties>();
		material = segProperties.Material;
		ApplyMaterial();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
