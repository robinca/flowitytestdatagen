﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActiveChildCycler : MonoBehaviour {

    [SerializeField]
    private List<Transform> ignoredTransforms;
	private int activeChildIndex = 0;
	private List<GameObject> children;
	// Use this for initialization
	void Start () {
		children = GetComponentsInChildren<Transform>(true).Where(t => !t.gameObject.Equals(gameObject) && !ignoredTransforms.Contains(t)).Select(t => t.gameObject).ToList();
		activeChildIndex = children.IndexOf(children.First(go => go.activeInHierarchy));
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Space))
		{
			children[activeChildIndex].SetActive(false);
			activeChildIndex = (activeChildIndex + 1) % children.Count;
			children[activeChildIndex].SetActive(true);

		}
	}
}
