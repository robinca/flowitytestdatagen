﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SegmentationProperties : MonoBehaviour {

	public enum ObjectType { Person, Object }
    [SerializeField]
	protected ObjectType objectType = ObjectType.Object;

	[SerializeField]
	protected Color segmentColor;

	[SerializeField]
	protected Shader shader;

	//[SerializeField]
	//protected RenderTexture renderTexture;

	public bool visualize = false;
	private Texture2D guiTexture;
	public Rect guiRect;

	// Use this for initialization
	void Start () {
		
		var	color = new Color(Random.value, Random.value, Random.value, 0.25f);
		guiTexture = new Texture2D(1, 1);
		guiTexture.SetPixel(0, 0, color);
		guiTexture.Apply();
	}
	
	// Update is called once per frame
	void Update () {

	}
	private void OnGUI()
	{
		if (!visualize)
			return;
		GUIStyle generic_style = new GUIStyle();
		GUI.skin.box = generic_style;
		GUI.skin.box.normal.background = guiTexture;
		GUI.Box(guiRect, gameObject.name, generic_style);
	}
    //private void OnDrawGizmos()
    //{
    //	Gizmos.DrawGUITexture(guiRect, guiTexture);
    //	Debug.Log(guiRect);
    //}
    //private Texture2D Texture
    //{
    //	get
    //	{
    //		var texture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
    //		RenderTexture.active = renderTexture;
    //		texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
    //		texture.Apply();
    //		return texture;
    //	}
    //}
    public VisibleObjectData GetVisibleObjectData(Color[] pixels, int textureWidth, int textureHeight)
    {
        return new VisibleObjectData
        {
            name = objectType.ToString(),
            boundingRect = this.GetBoundingRect(pixels, textureWidth, textureHeight)//GetRectangle(Texture)
        };
    }

 //   public VisibleObjectData VisibleObjectData
	//{
	//	get
	//	{
	//		return new VisibleObjectData
	//		{
	//			name = objectType.ToString(),
	//			boundingRect = GetRectangle(Texture)
	//		};
	//	}
	//}
	public Material Material
	{
		get
		{
			var mat = new Material(shader);
			mat.SetColor("_Color", segmentColor);
			return mat;
		}
	}
    public Rect GetRectangle(Texture2D fromTexture)
	{
		var rect = FindBoundingRectFromSegmentation.GetBoundingRectFromSegmentationTexture(fromTexture, segmentColor);
		guiRect = rect;
		
		return rect;
		
	}
    public Color SegmentColor => segmentColor;
}
