﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Patroller : MonoBehaviour
{
	public List<Transform> wayPoints;
	private int targetIndex=0;
	private NavMeshAgent agent;
	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();
		agent.destination = wayPoints.First().position;
	}
	
	// Update is called once per frame
	void Update () {
		if(!agent.pathPending && !agent.hasPath)
		{
			targetIndex = (targetIndex+1) % wayPoints.Count;
			agent.destination = wayPoints[targetIndex].position;
		}
	}
}
