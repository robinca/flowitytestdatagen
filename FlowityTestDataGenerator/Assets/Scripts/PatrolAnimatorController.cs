﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(PatrolRandomPointsOnNavMesh))]
public class PatrolAnimatorController : MonoBehaviour {
	
	public List<Animator> animators;
	// Use this for initialization
	void Start () {
		if (!animators.Any())
		{
			Debug.Log($"Animator on {gameObject.name} has not been set");
			return;
		}
		var patrolRandomPointsOnNav = GetComponent<PatrolRandomPointsOnNavMesh>();
		patrolRandomPointsOnNav.PatrolPointFound += () => 
		{
			animators.ForEach(a => a.SetBool("walking", true));
		};
		patrolRandomPointsOnNav.PatrolPointReached += () =>
		{
			animators.ForEach(a => a.SetBool("walking", false));
		};
	}
}
