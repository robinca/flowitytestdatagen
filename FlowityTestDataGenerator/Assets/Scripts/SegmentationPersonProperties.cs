﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using M3D_DLL;

public class SegmentationPersonProperties : SegmentationProperties {
	private static int personIndex = 0;

	private void Awake()
	{
		segmentColor = new Color(0, (float)personIndex / 255.0f, 1, 1);
		personIndex++;
	}
	// Update is called once per frame
	void Update () {
		
	}
	
}
