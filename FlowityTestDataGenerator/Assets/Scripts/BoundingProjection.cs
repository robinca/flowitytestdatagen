﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Renderer))]
public class BoundingProjection : MonoBehaviour {

	public bool visualize = false;
	[SerializeField]
	private string objectType = "person";
	[SerializeField]
	private Color color;
	[SerializeField]
	private bool randomColor = true;


	private Collider collider;
	private Rect rect;
	private Texture2D texture;
	private Texture2D pointTexture;
	private Vector3[] corners;

	public Rect BoundingRectangle => BoundsToScreenRect(collider.bounds);
	public VisibleObjectData VisibleObjectData
	{
		get
		{
			return new VisibleObjectData
			{
				name = objectType,
				boundingRect = BoundsToScreenRect(collider.bounds),
				isVisible = IsWithinView
			};
		}
	}

	private bool IsWithinView
	{
		get
		{
			return corners.Any(c =>
			{
				var x = c.x / Screen.width;
				var y = c.y / Screen.height;
				return x > 0 && x < 1 && y > 0 && y < 1 && c.z > Camera.main.nearClipPlane;
			});
		}
	}
	// Use this for initialization
	void Start () {
		if (randomColor)
			color = new Color(Random.value, Random.value, Random.value, 0.25f);
		collider = GetComponent<Collider>();
		texture = new Texture2D(1, 1);
		texture.SetPixel(0, 0,color);
		texture.Apply();
		pointTexture = new Texture2D(1, 1);
		pointTexture.SetPixel(0, 0, new Color(1, 0, 1, 1));
		pointTexture.Apply();
	}
	
	private void OnGUI()
	{
		if (!visualize)
			return;
		GUIStyle generic_style = new GUIStyle();
		GUI.skin.box = generic_style;
		GUI.skin.box.normal.background = texture;
		GUI.Box(BoundingRectangle, gameObject.name, generic_style);
		GUI.skin.box.normal.background = pointTexture;
		foreach (var c in corners)
		{
			GUI.Box(new Rect(c.x, Screen.height - c.y, 2, 2), "", generic_style);
		}
	}
	
	private Rect BoundsToScreenRect(Bounds bounds)
	{
		corners = GetBoundsCorners(bounds).Select(c => Camera.main.WorldToScreenPoint(c)).ToArray();
		var minX = corners.OrderBy(c => c.x).First().x;
		var maxX = corners.OrderBy(c => c.x).Last().x;
		var minY = corners.OrderBy(c => c.y).First().y;
		var maxY = corners.OrderBy(c => c.y).Last().y;
		return new Rect(minX, Screen.height-maxY, maxX-minX, maxY-minY);
	}
	private Vector3[] GetBoundsCorners(Renderer[] rends)
	{
		return rends.SelectMany(r => GetBoundsCorners(r.bounds)).ToArray();
	}
	private Vector3[] GetBoundsCorners(Bounds bounds)
	{
		return new Vector3[]
		{
			bounds.min,
			bounds.max,
			new Vector3(bounds.min.x, bounds.min.y, bounds.max.z),
			new Vector3(bounds.min.x, bounds.max.y, bounds.min.z),
			new Vector3(bounds.max.x, bounds.min.y, bounds.min.z),
			new Vector3(bounds.max.x, bounds.max.y, bounds.min.z),
			new Vector3(bounds.max.x, bounds.min.y, bounds.max.z),
			new Vector3(bounds.min.x, bounds.max.y, bounds.max.z)
		};
	}
}
