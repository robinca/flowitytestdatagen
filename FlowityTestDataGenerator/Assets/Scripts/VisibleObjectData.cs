﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct VisibleObjectData {
	public string name;
	public bool isVisible;
	public Rect boundingRect;
}
