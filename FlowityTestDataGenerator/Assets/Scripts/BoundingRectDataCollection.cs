﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using System;
using UnityEngine;
using UnityEngine.UI;

public class BoundingRectDataCollection : MonoBehaviour {

    [SerializeField]
    private float minimumRectangleSize = 1.0f;
    [SerializeField]
    private float timeBetweenDataCaptures = 1.0f;
    [SerializeField]
    private int dataCaptureCount = 60;
    ////[SerializeField]
    private string filePath;
    [SerializeField]
    private string filePrefix = "dataCapture";

    [SerializeField]
    private RenderTexture renderTexture;

    private string baseDir = "GenerateDataResult";
    private string imagesDirectory = "Images";
    private string flowityDirectory = "Flowity";
    private string darknetDirectory = "DarkNet";
    private float currentCaptureTime = 0.0f;
    private int currentCaptureCount = 0;
    private List<SegmentationProperties> segmentationProperties;
    private XmlWriterSettings xmlWriterSettings;
    private bool startCapture = false;


    void Start() {

        

        xmlWriterSettings = new XmlWriterSettings();
        xmlWriterSettings.NewLineOnAttributes = true;
        xmlWriterSettings.Indent = true;
        segmentationProperties = FindObjectsOfType<SegmentationProperties>().ToList();
        if (!Directory.Exists($"{Application.dataPath}\\{baseDir}"))
            Directory.CreateDirectory($"{Application.dataPath}\\{baseDir}");
        filePath = $"{Application.dataPath}\\{baseDir}";
        if (!Directory.Exists($"{filePath}\\{flowityDirectory}"))
            Directory.CreateDirectory($"{filePath}\\{flowityDirectory}");
        if (!Directory.Exists($"{filePath}\\{darknetDirectory}"))
            Directory.CreateDirectory($"{filePath}\\{darknetDirectory}");
        if (!Directory.Exists($"{filePath}\\{imagesDirectory}"))
            Directory.CreateDirectory($"{filePath}\\{imagesDirectory}");
    }
    
    // Update is called once per frame
    void Update() {
        
        if (!StateHandler.Instance.CurrentAppState.Equals(StateHandler.AppState.RunDataCollection))
            return;
        if (Input.GetKeyUp(KeyCode.P))
        {
            startCapture = true;
        }
        currentCaptureTime += Time.deltaTime;
        if (currentCaptureTime >= timeBetweenDataCaptures && currentCaptureCount < dataCaptureCount)
        {
            currentCaptureTime -= timeBetweenDataCaptures;
            startCapture = true;
        }
    }
    private void OnPostRender()
    {
        if (startCapture)
        {
            var imagePath = $"{filePath}\\{imagesDirectory}\\{filePrefix}{currentCaptureCount}.png";
            var flowityPath = $"{filePath}\\{flowityDirectory}\\{filePrefix}{currentCaptureCount}.xml";
            var darknetPath = $"{filePath}\\{darknetDirectory}\\{filePrefix}{currentCaptureCount}.txt";
            Debug.Log("Data Capture taken " + currentCaptureCount);
            DataCapture(imagePath, flowityPath, darknetPath);
            startCapture = false;
        }
    }
    private Texture2D GetTexture()
    {
        var outputTexture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
        RenderTexture.active = renderTexture;
        outputTexture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        outputTexture.Apply();
        return outputTexture;   
    }
    public void SetCaptureCount(InputField input)
    {
        dataCaptureCount = int.Parse(input.text);
    }
    private void DataCapture(string imagePath, string flowityPath, string darknetPath)
    {
        ScreenCapture.CaptureScreenshot(imagePath);
        var texture = GetTexture();
        var pixels = texture.GetPixels();
        var data = segmentationProperties
            .Select(p => p.GetVisibleObjectData(pixels, texture.width, texture.height))
            .Where(d => d.boundingRect.size.magnitude > minimumRectangleSize).ToArray();
        Destroy(texture);
        SaveToXML(data, imagePath, flowityPath);
        SaveDarkNetAnnotation(data, darknetPath);
        currentCaptureCount++;
        if (currentCaptureCount.Equals(dataCaptureCount))
        {
            Debug.Log("Data Capture Complete");
            if (!Debug.isDebugBuild)
                Application.Quit();
        }
    }
    private void SaveDarkNetAnnotation(VisibleObjectData[] data, string annotationPath)
    {
        using (StreamWriter file = new StreamWriter(annotationPath))
        {
            foreach (var d in data)
            {
                var rect = ConvertRectToDarkNetRect(d.boundingRect);
                file.WriteLine($"{(int)Enum.Parse(typeof(SegmentationProperties.ObjectType), d.name)} " +
                    $"{rect.x.ToString().Replace(',', '.')} " +
                    $"{rect.y.ToString().Replace(',', '.')} " +
                    $"{rect.width.ToString().Replace(',', '.')} " +
                    $"{rect.height.ToString().Replace(',', '.')}");
            }
        }

    }
    private Rect ConvertRectToDarkNetRect(Rect rect)
    {
        var dw = 1.0f / Screen.width;
        var dh = 1.0f / Screen.height;
        var x = (rect.xMin + rect.xMax) * 0.5f * dw;
        var y = (rect.yMin + rect.yMax) * 0.5f * dh;
        var w = rect.width*dw;
        var h = rect.height*dh;
        return new Rect(x, y, w, h);
}
    private void SaveToXML(VisibleObjectData[] data, string imagePath, string annotationPath)
	{
		using (XmlWriter writer = XmlWriter.Create(annotationPath, xmlWriterSettings))
		{
			writer.WriteStartDocument();
			writer.WriteStartElement("annotation");

			var splitPath = imagePath.Split("\\".ToCharArray());
			writer.WriteStartElement("folder");
			writer.WriteString(splitPath[splitPath.Length - 2]);
			writer.WriteEndElement();
			writer.WriteStartElement("filename");
			writer.WriteString(splitPath.Last());
			writer.WriteEndElement();

			writer.WriteStartElement("size");
			writer.WriteStartElement("width");
			writer.WriteString(renderTexture.width.ToString());
			writer.WriteEndElement();
			writer.WriteStartElement("height");
			writer.WriteString(renderTexture.width.ToString());
			writer.WriteEndElement();
			writer.WriteStartElement("depth");
			writer.WriteString(renderTexture.depth.ToString());
			writer.WriteEndElement();
			writer.WriteEndElement();
            
			foreach (var d in data)
			{
				writer.WriteStartElement("object");
				writer.WriteStartElement("name");
				writer.WriteString(d.name);
				writer.WriteEndElement();
				writer.WriteStartElement("bndbox");
				writer.WriteStartElement("xmin");
				writer.WriteString(d.boundingRect.x.ToString());
				writer.WriteEndElement();
				writer.WriteStartElement("ymin");
				writer.WriteString(d.boundingRect.y.ToString());
				writer.WriteEndElement();
				writer.WriteStartElement("xmax");
				writer.WriteString((d.boundingRect.x + d.boundingRect.width).ToString());
				writer.WriteEndElement();
				writer.WriteStartElement("ymax");
				writer.WriteString((d.boundingRect.y + d.boundingRect.height).ToString());
				writer.WriteEndElement();
				writer.WriteEndElement();
				writer.WriteEndElement();
			}

			writer.WriteEndElement();
			writer.WriteEndDocument();
		}
	}
}
