﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ApplyMaterialToRenderersInHierarchy : MonoBehaviour {

	[SerializeField]
	protected Material material;

	// Use this for initialization
	void Start () {
		ApplyMaterial();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	protected void ApplyMaterial()
	{
		var rends = GetBloodLine(new[] { transform }).Select(t => t.GetComponent<Renderer>()).Where(r => r != null).ToList();
		rends.ForEach(r =>
		{
			var materials = new Material[r.materials.Length];
			for (int i = 0; i < r.materials.Length; i++)
			{
				materials[i] = material;
			}
			r.materials = materials;
		});
	}
	private Transform[] GetBloodLine(Transform[] foundChildren)
	{
		if (foundChildren.All(c => c.transform.childCount.Equals(0)))
			return foundChildren;
		var children = foundChildren.SelectMany(c => c.GetComponentsInChildren<Transform>()).Except(foundChildren).ToArray();
		return foundChildren.Concat(GetBloodLine(children)).ToArray();
	}
}
