﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

public static class FindBoundingRectFromSegmentation
{
    public static Rect GetBoundingRectFromPixels(Color[] fromPixels, Color color, int textureWidth, int textureHeight)
    {
        var hotPixelIndices = new List<int>();
        for (int i = 0; i < fromPixels.Length; i++)
        {
            if (((Color32)fromPixels[i]).Equals((Color32)color))
                hotPixelIndices.Add(i);
        }
        if (!hotPixelIndices.Any())
            return new Rect();
        var hotPixelsByWidth = hotPixelIndices.Select(p => p % textureWidth).OrderBy(p => p).ToArray();
        var hotPixelsByHeight = hotPixelIndices.Select(p => Mathf.CeilToInt(p / textureWidth)).OrderBy(p => p).ToArray();
        var width = hotPixelsByWidth.Last() - hotPixelsByWidth.First();
        var height = hotPixelsByHeight.Last() - hotPixelsByHeight.First();
        return new Rect(hotPixelsByWidth.First(), textureHeight - hotPixelsByHeight.First() - height, width, height);
    }
    public static Rect GetBoundingRectFromSegmentationTexture(Texture2D fromTexture, Color color)
	{
        return GetBoundingRectFromPixels(fromTexture.GetPixels(), color, fromTexture.width, fromTexture.height);
		//var pixels = fromTexture.GetPixels().ToList();
		//var hotPixelIndices = new List<int>();
		//for(int i = 0; i < pixels.Count; i++)
		//{
		//	if (((Color32)pixels[i]).Equals((Color32)color))
		//		hotPixelIndices.Add(i);
		//}
		//if (!hotPixelIndices.Any())
		//	return new Rect();
		//var hotPixelsByWidth = hotPixelIndices.Select(p => p % fromTexture.width).OrderBy(p => p).ToArray();
		//var hotPixelsByHeight = hotPixelIndices.Select(p => Mathf.CeilToInt(p / fromTexture.width)).OrderBy(p => p).ToArray();
		//var width = hotPixelsByWidth.Last() - hotPixelsByWidth.First();
		//var height = hotPixelsByHeight.Last() - hotPixelsByHeight.First();
		//return new Rect(hotPixelsByWidth.First(), fromTexture.height - hotPixelsByHeight.First() - height, width, height);
	}
    public static Rect GetBoundingRect(this SegmentationProperties segmentationProperties, Color[] fromPixels, int textureWidth, int textureHeight)
    {
        return GetBoundingRectFromPixels(fromPixels, segmentationProperties.SegmentColor, textureWidth, textureHeight);
    }
}
