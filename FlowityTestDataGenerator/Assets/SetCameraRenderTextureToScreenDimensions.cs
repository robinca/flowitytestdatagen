﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class SetCameraRenderTextureToScreenDimensions : MonoBehaviour {
	
	// Use this for initialization
	void Awake () {
		var cam = GetComponent<Camera>();
		cam.targetTexture.width = Screen.width;
		cam.targetTexture.height = Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
