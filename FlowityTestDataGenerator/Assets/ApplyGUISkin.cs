﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyGUISkin : MonoBehaviour
{
    [SerializeField]
    private GUISkin skin;
    private void OnGUI()
    {
        GUI.skin = skin;
    }
}
