﻿Shader "Custom/ColourByStencilShader" {
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("", 2D) = "white" {}
		_StencilReadMask("Stencil Read Mask", Range(0,255)) = 0.0
	}

	SubShader
	{
		Pass
		{
			Stencil
			{
				Ref[_StencilReadMask]
				Comp equal
			}
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			// vertex shader inputs
			struct appdata
			{
				float4 vertex : POSITION; // vertex position
				float2 uv : TEXCOORD0; // texture coordinate
			};
			struct v2f
			{
				float2 uv : TEXCOORD0; // texture coordinate
				float4 vertex : SV_POSITION; // clip space position
			};

			// vertex shader
			v2f vert(appdata v)
			{
				v2f o;
				// transform position to clip space
				// (multiply with model*view*projection matrix)
				o.vertex = UnityObjectToClipPos(v.vertex);
				// just pass the texture coordinate
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			float4 _Color;
			//Fragment shader
			float4 frag(v2f i) : COLOR
			{
				//float3 col = tex2D(_MainTex, float2(i.uv.x, i.uv.y));
				return _Color;//float4(col.r,col.g,col.b, 1.0);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
