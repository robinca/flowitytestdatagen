﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/FishEyeShaderProto"
{
	Properties
	{
		_MainTex("", 2D) = "white" {}
		_Distortion("_Distortion", Range(-1.45, 1.45)) = 1
	}

	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			// vertex shader inputs
			struct appdata
			{
				float4 vertex : POSITION; // vertex position
				float2 uv : TEXCOORD0; // texture coordinate
			};
			struct v2f
			{
				float2 uv : TEXCOORD0; // texture coordinate
				float4 vertex : SV_POSITION; // clip space position
			};

			// vertex shader
			v2f vert(appdata v)
			{
				v2f o;
				// transform position to clip space
				// (multiply with model*view*projection matrix)
				o.vertex = UnityObjectToClipPos(v.vertex);
				// just pass the texture coordinate
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			float _Distortion;
			//Fragment shader
			float4 frag(v2f i) : COLOR
			{
				half2 p = i.uv;
				half2 m = half2(0.5, 0.5);
				half2 d = p - m;
				float r = sqrt(dot(d, d));
				half2 uv;
				float3 col = float3(0.0, 0.0, 0.0);
				// SQUAREXY:
				//uv = m + half2(d.x * abs(d.x), d.y * abs(d.y));
				// SQUARER:
				//uv = m + d * r; // a.k.a. m + normalize(d) * r * r
				// ASINR:
				uv = m + normalize(d) * asin(r) / (3.14159 * 0.5)*_Distortion;
				col = tex2D(_MainTex, float2(uv.x, uv.y));
				return float4(col.r,col.g,col.b, 1.0);
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
