﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class OnlyActiveDuringCertainStates : MonoBehaviour
{
    [SerializeField]
    private List<StateHandler.AppState> activeAppStates;

    // Start is called before the first frame update
    void Awake()
    {
        StateHandler.Instance.StateChanged += OnStateChanged;
    }
    private void OnStateChanged(StateHandler.AppState currentState)
    {
        gameObject.SetActive(activeAppStates.Contains(currentState));
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
