﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class StateHandler : MonoBehaviour
{
    private static StateHandler _instance;

    public static StateHandler Instance { get { return _instance; } }

    [System.Flags]
    public enum AppState
    {
        InputVariables,
        RunDataCollection,
    };
    public delegate void ChangedStateEvent(AppState appState);
    public ChangedStateEvent StateChanged;

    [SerializeField]
    private AppState currentAppState;

    public AppState CurrentAppState
    {
        get { return currentAppState; }
        set
        {
            currentAppState = value;
            StateChanged(currentAppState);
            Debug.Log("CurrentAppState changed to " + currentAppState.ToString());
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
    }
    private void Update()
    {
        //if(Input.GetKeyUp(KeyCode.S))
        //{
        //    CurrentAppState = (AppState)(((int)CurrentAppState + 1) % Enum.GetNames(typeof(AppState)).Length);
            
        //}
    }
    public void StartDataCollection()
    {
        CurrentAppState = AppState.RunDataCollection;
    }
}
